// Imprima los primeros 10 números de Fibonacci.

package main

import "fmt"

func main() {
	primero := 0
	segundo := 1
	siguiente := 0
	n := 10

	fmt.Print("Secuencia Fibonacci:")

	for i := 1; i <= n; i++ {
		if i == 1 {
			fmt.Print(" ", primero)
			continue
		}
		if i == 2 {
			fmt.Print(" ", segundo)
			continue
		}
		siguiente = primero + segundo
		primero = segundo
		segundo = siguiente
		fmt.Print(" ", siguiente)
	}

}
