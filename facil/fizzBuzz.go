// Fizz-buzz: reemplace los múltiplos de 3 con Fizz, múltiplos de 5 con Buzz y múltiplos de
// ambos con FizzBuzz. Imprima los primeros 10 valores.

package main

import (
	"fmt"
)

func main() {

	cantidadValores := 10
	for i := 1; i <= cantidadValores; i++ {
		fizzbuzz(i)
	}
}

func fizzbuzz(i int) {
	fizz := "fizz"
	buzz := "buzz"

	if i%3 == 0 && i%5 == 0 { // múltiplos de ambos
		fmt.Println(i, fizz+buzz)
	} else if i%3 == 0 { // múltiplos de 3
		fmt.Println(i, fizz)
	} else if i%5 == 0 { // múltiplos de 5
		fmt.Println(i, buzz)
	} else { // no múltiplo
		fmt.Println(i)
	}
}
