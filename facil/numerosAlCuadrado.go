// Imprima los primeros 10 números al cuadrado.

package main

import "fmt"

func main() {
	for i := 1; i <= 10; i++ {
		fmt.Println("(", i, ") ^ 2 =", i*i)
	}
}