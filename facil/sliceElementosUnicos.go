// Escriba una función que devuelva el número de elementos únicos en un slice, demuestre su uso.

package main

import (
	"fmt"
)

func borrarDuplicados(sliceOriginal []int) []int {
	claves := make(map[int]bool)
	listaSinDup := []int{}

	for _, entrada := range sliceOriginal {
		if _, value := claves[entrada]; !value {
			claves[entrada] = true
			listaSinDup = append(listaSinDup, entrada)
		}
	}
	return listaSinDup
}

func main() {

	slice1 := []int{1, 2, 3, 4, 5, 2, 3, 5, 7, 9, 6, 7}

	fmt.Println("Slice original")
	fmt.Println(slice1)

	slice2 := borrarDuplicados(slice1)

	fmt.Println("Slice sin duplicados")
	fmt.Println(slice2)
	fmt.Println("Cantidad de elementos únicos: ", len(slice2))
}
