// Escriba una función que invierta un slice, demuestre su uso.

package main

import (
	"fmt"
	"reflect"
)

func reverseGo118[S ~[]E, E any](s S) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}

func reverseGo18(s interface{}) {
	n := reflect.ValueOf(s).Len()
	swap := reflect.Swapper(s)
	for i, j := 0, n-1; i < j; i, j = i+1, j-1 {
		swap(i, j)
	}
}

func main() {
	s := []interface{}{1, "2", uint(3), byte(4), float64(5)}
	reverseGo18(s)
	fmt.Println(s)
	reverseGo118(s)
	fmt.Println(s)
}

